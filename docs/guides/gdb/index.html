<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Guide to using GDB in simulation mode</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Guide to using GDB in simulation mode</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    <p>The GDB debugger is a superb tool for observing and manipulating a
running program. Becoming facile with a full-featured debugger such as
<code class="highlighter-rouge">gdb</code> adds a critical superpower to your effectiveness as a software engineer.</p>

<p>In our bare metal world, the debugger options are more
limited than they would be in a hosted environment. You will not be able to
monitor and interact with your program while it is actually executing on the
Raspberry Pi. However, you can use the debugger in simulation mode. You run <code class="highlighter-rouge">gdb</code> on your laptop and execute your program inside the debugger using the built-in ARM simulator.
Under simulation, you can single-step through your code, set breakpoints, print variables, examine registers and memory, and more. These observations can help you to understand what your code is doing and diagnose bugs.</p>

<p>Using the debugger is largely the same whether in simulation mode or not, but some programs may execute differently under simulation than when running on the actual Pi. Be sure to read the section below on those <a href="#simulation">differences due to simulation</a>.</p>

<h3 id="sample-gdb-session">Sample gdb session</h3>
<p>Let’s try out the gdb simulator on the following C program that blinks an LED.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>static volatile unsigned int *FSEL2 = (unsigned int *)0x20200008;
static volatile unsigned int *SET0  = (unsigned int *)0x2020001C;
static volatile unsigned int *CLR0  = (unsigned int *)0x20200028;

void main(void)
{
    *FSEL2 = 1;

    while (1) {
        *SET0 = 1 &lt;&lt; 20;
        for (int i = 0; i &lt; 0x3f0000; i++) ;
        *CLR0 = 1 &lt;&lt; 20;
        for (int i = 0; i &lt; 0x3f0000; i++) ;
    }
}
</code></pre></div></div>

<p>The <code class="highlighter-rouge">blink.elf</code> file is typically the penultimate step in our build, right 
before we extract the raw binary instructions into the <code class="highlighter-rouge">blink.bin</code> that is
sent to the Pi.  The <code class="highlighter-rouge">elf</code> version of the file is the one used by the gdb simulator.</p>

<p>Run <code class="highlighter-rouge">gdb</code> on the <code class="highlighter-rouge">blink.elf</code> file:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ arm-none-eabi-gdb blink.elf
GNU gdb (GDB) 7.8.1
Copyright (C) 2014 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later &lt;http://gnu.org/licenses/gpl.html&gt;
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-apple-darwin14.0.0 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
&lt;http://www.gnu.org/software/gdb/bugs/&gt;.
Find the GDB manual and other documentation resources online at:
&lt;http://www.gnu.org/software/gdb/documentation/&gt;.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from blink.elf...done.
(gdb)
</code></pre></div></div>

<p>In gdb, we first connect to the simulator:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) target sim
Connected to the simulator
</code></pre></div></div>

<p>And then load the program:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) load
Loading section .text, size 0xd8 vma 0x8000
Start address 0x8000
Transfer rate: 1728 bits in &lt;1 sec.
</code></pre></div></div>

<p><code class="highlighter-rouge">gdb</code> prints the size of the program (0xd8 bytes) and
the start address (0x8000).</p>

<p>Let’s review the code for <code class="highlighter-rouge">main</code>. Note that <code class="highlighter-rouge">gdb</code> knows about the source file and line numbers because our Makefile uses compiler flag <code class="highlighter-rouge">-g</code>.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) list main
1   static volatile unsigned int *FSEL2 = (unsigned int *)0x20200008;
2   static volatile unsigned int *SET0  = (unsigned int *)0x2020001C;
3   static volatile unsigned int *CLR0  = (unsigned int *)0x20200028;
4   
5   void main(void)
6   {
7       *FSEL2 = 1;
8       
9       while (1) {
10          *SET0 = 1 &lt;&lt; 20;
</code></pre></div></div>

<p>Set a breakpoint on line 10 of this file:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) break 10
Breakpoint 1 at 0x8028: file blink.c, line 10.
</code></pre></div></div>

<p>The <code class="highlighter-rouge">run</code> command starts executing the program in the simulator. It will quickly hit the breakpoint we set:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) run
Starting program: blink.elf
Breakpoint 1, main () at blink.c:10
10          *SET0 = 1 &lt;&lt; 20;
</code></pre></div></div>

<p>The program is stopped at line 10. This is before this line of C has executed.</p>

<p>The gdb <code class="highlighter-rouge">print</code> command can be used to view a variable or evaluate an expression.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) print SET0
$1 = (volatile unsigned int * const) 0x2020001c
(gdb) print *SET0
$2 = 0
</code></pre></div></div>

<p>Execute the next line.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) next
</code></pre></div></div>

<p>Print the affected memory location to see its updated value. <code class="highlighter-rouge">print/x</code> says to print in hexadecimal format.</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>(gdb) print/x *SET0
$3 = 0x100000
</code></pre></div></div>

<p>Success. The assignment statement turned on bit 20!</p>

<p>Use <code class="highlighter-rouge">next</code> to execute the next line, the delay loop. The simulator operates fairly slowly, so the loop will take a while to execute. Use <code class="highlighter-rouge">Control-C</code> to interrupt the program and bring control back to the debugger.  Print the loop counter to see how far into the loop it is:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>^C0x0000804c in main () at blink.c:11
11          for (int i = 0; i &lt; 0x3f0000; i++) 
(gdb) print i
$4 = 22928
</code></pre></div></div>

<p>Use <code class="highlighter-rouge">cont</code> to let the program resume execution.</p>

<p><a name="simulation"></a></p>
<h3 id="differences-due-to-simulation">Differences due to simulation</h3>

<p>It’s important to note that running under the simulator is not the same as running on the actual Raspberry Pi. 
The simulator does not model the peripherals such as GPIO or timer. For example,
the blink program above drives an LED connected to GPIO 20. If you run this program in the
simulator, the LED will not light. The simulator is not talking to your
Raspberry Pi (you won’t even need your Pi to be plugged in), nor is the simulator doing
anything special when your program accesses the memory locations for the memory-mapped peripherals. You can step through the blink program under the simulator and examine the state, but the code that attempts to control the peripherals is a no-op – no LED will light, the timer does not increment.</p>

<p>Another important issue to be aware of is that the default state of registers and memory may be different under the simulator. For a correctly-written program, this difference is of no consequence, but a buggy program can be sensitive to it; such as a program that mistakenly accesses memory out of bounds or uses an uninitialized variable. The irony is that such buggy programs are exactly the ones that you will need the debugger’s help to resolve, yet, frustratingly, these programs can exhibit different behavior when run under the simulator than when running on the Pi. If running in the gdb simulator, the contents of not-yet-accessed memory defaults to zero. If running the program on the actual Raspberry Pi, the contents of unaccessed memory is unpredictable. Consider a program containing the following (buggy) function:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>int gauss(int n)
{
    int result;             // oops! never initialized
    for (int i = 1; i &lt;= n; i++)
        result += i;
    return result;
}
</code></pre></div></div>

<p><code class="highlighter-rouge">result</code> is used uninitialized. If the stack variable is stored in a region of memory as-yet-untouched, its initial contents are garbage. When running on the Pi, you observe the value returned by the function to be unpredictable due to the missing initialization.</p>

<p>You now try running the same program under the gdb simulator and the first call to the function returns the correct answer because the intended initialization to zero is being supplied by the simulator. The memory contents in the simulator are retained between runs, so if you run the program again without having left the simulator, this time <code class="highlighter-rouge">result</code> is “initialized” to its last value and function now returns an incorrect result.</p>

<p>Arg! These conflicting observations can be mystifying and you may start to think you are losing your mind. The difference in environment is changing the observed effect of the bug. In every case, the program is buggy; the fact that it surfaces differently is further evidence of its errant ways. It may take some digging to sort things out, but it is a transcendent experience when you can precisely trace the cause and effect and how it resulted in the observed behaviors. Having attained a complete understanding, you can construct an appropriate fix so that the program runs correctly in all contexts.</p>

<p>Despite its limitations, gdb simulation mode can be a powerful ally when dealing with difficult bugs. Learn to make good use of this tool, but do stay mindful of the ways in which it is not an exact match to running on the actual Raspberry Pi.</p>

<h3 id="common-commands">Common commands</h3>

<p>Here is a list of useful <code class="highlighter-rouge">gdb</code> commands. Many command names can be abbreviated. For example, <code class="highlighter-rouge">info registers</code> can be invoked as <code class="highlighter-rouge">i r</code>, <code class="highlighter-rouge">stepi</code> can be invoked <code class="highlighter-rouge">si</code>, <code class="highlighter-rouge">print/x</code> is shortened to <code class="highlighter-rouge">p/x</code>, and so on.</p>

<table>
  <tbody>
    <tr>
      <td><strong>Command</strong>                 </td>
      <td><strong>Use to</strong></td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">run</code></td>
      <td>start program executing from beginning</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">Control-C</code></td>
      <td>interrupt the executing program, give control back to gdb</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">continue</code></td>
      <td>resume program execution</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">break WHERE</code></td>
      <td>set breakpoint to stop at, <code class="highlighter-rouge">WHERE</code> is name of function or line number or address of instruction</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">info break</code></td>
      <td>list all breakpoints</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">delete [N]</code></td>
      <td>remove n’th breakpoint, with no argument removes all breakpoints</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">stepi</code></td>
      <td>execute next assembly instruction</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">step</code></td>
      <td>execute the next line of C source, step into function call</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">next</code></td>
      <td>execute the next line of C source, step over function calls</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">where</code></td>
      <td>show stack backtrace up to current execution</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">disassemble [WHAT]</code></td>
      <td>disassemble instructions, <code class="highlighter-rouge">WHAT</code> can be function name or address, if no arg disassemble currently executing function</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">info registers</code></td>
      <td>show contents of all registers</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">print/d EXPR</code></td>
      <td>eval expression, print result in decimal</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">print/x EXPR</code></td>
      <td>eval expression, print result in hex</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">print/t EXPR</code></td>
      <td>eval expression in binary, print result in binary</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">display EXPR</code></td>
      <td>auto-print expression after each command</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">info display</code></td>
      <td>list all auto-display expressions</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">undisplay [n]</code></td>
      <td>remove n’th auto-display expression</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">x/HOW ADDR</code></td>
      <td>examine contents of memory at ADDR, use format <code class="highlighter-rouge">HOW</code> 3 letters for repeatcount, format, size</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">help [cmdname]</code></td>
      <td>show help for gdb command <code class="highlighter-rouge">[cmdname]</code></td>
    </tr>
    <tr>
      <td>⬆️⬇️</td>
      <td>scroll back/forward through previously executed commands</td>
    </tr>
    <tr>
      <td><code class="highlighter-rouge">quit</code></td>
      <td>quit gdb</td>
    </tr>
  </tbody>
</table>

<h3 id="configure-using-gdbinit-files">Configure using .gdbinit files</h3>

<p>GDB can ask a lot of annoying questions and does not track previous commands
by default.  I put the following in a <code class="highlighter-rouge">.gdbinit</code> configuration file 
in my home directory to fix that:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cat ~/.gdbinit 
set confirm off
set history expansion
</code></pre></div></div>

<p>Whenever you start gdb, the commands from your <code class="highlighter-rouge">~/.gdbinit</code> configuration 
file are read and executed. These user defaults are useful for any
run of gdb.</p>

<p>For a program that runs only in simulation mode, you might want to
avoid having to repeatedly set the target and load the program each time you use gdb. For this, 
add a <code class="highlighter-rouge">.gdbinit</code> configuration file in the project directory:</p>

<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cat ./.gdbinit
target sim
load
</code></pre></div></div>

<p>Whenever you start gdb in this directory, the commands in the local
<code class="highlighter-rouge">./.gdbinit</code> file will set the simulator target and load the program
automatically.</p>

<h3 id="additional-resources">Additional resources</h3>

<ul>
  <li>CS107’s <a href="https://web.stanford.edu/class/archive/cs/cs107/cs107.1186/guide/gdb.html">guide to gdb</a> is a good introduction.</li>
  <li>Watch Chris Gregg’s <a href="https://www.youtube.com/watch?v=uhIt8YqtmuQ&amp;feature=youtu.be">video tutorial on gdb</a>.</li>
  <li>Looking to learn some fancier tricks? See these articles Julie wrote for a 
programming journal: <a href="https://web.stanford.edu/class/archive/cs/cs107/cs107.1186/resources/gdb_coredump1.pdf">Breakpoint Tricks</a> 
and <a href="https://web.stanford.edu/class/archive/cs/cs107/cs107.1186/resources/gdb_coredump2.pdf">gdb’s Greatest Hits</a>.</li>
  <li>Last but not least, the full online gdb manual tells all: 
<a href="http://sourceware.org/gdb/current/onlinedocs/gdb/index.html">http://sourceware.org/gdb/current/onlinedocs/gdb/index.html</a>.</li>
</ul>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>