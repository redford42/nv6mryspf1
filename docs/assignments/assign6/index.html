<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Assignment 6: Graphics Library and Console</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Assignment 6: Graphics Library and Console</h1><hr>
    
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="main_for_toc">
    
<p><em>Written by Philip Levis, updated by Julie Zelenski</em></p>

<!-- If duedate set in front_matter, use that, otherwise calculate based on n*weeks + assign1 due -->

<p><b>Due: Tuesday, February 25 at  6:00 pm</b></p>

<p><img src="images/wargames.jpg" alt="Wargames Console" /></p>

<p>A console provides a command-line text interface for entering commands and seeing output.
Today we have fancy shell programs that support scripts, process
control, and output redirection. But simpler consoles can be
powerful too. One very famous console is
<a href="https://www.youtube.com/watch?v=ecPeSmF_ikc">Joshua in WarGames</a>.</p>

<h3 id="goals">Goals</h3>

<p>In this assignment, you will add graphics capability to your Pi and use it to create a snazzy graphical display for your shell. This will unleash your Raspberry Pi from the shackles of its laptop minder and elevate it into a standalone personal computer running a console that allows the user to enter and execute commands. Neat!</p>

<p>In completing this assignment you will have:</p>

<ul>
  <li>learned how a framebuffer is used as a bitmap to drive a video display</li>
  <li>implemented simple drawing routines</li>
  <li>unlocked achievement of wizard-level proficiency with C pointers and multi-dimensioned arrays</li>
</ul>

<p>After this there is one final assignment where you’ll polish up a few details and improve the performance and then your top-to-bottom system will be complete. This is all bare-metal code you wrote yourself – what an exciting achievement and sense of satisfaction you have earned with all your hard work!</p>

<h2 id="get-starter-files">Get starter files</h2>
<p>Change to the <code class="highlighter-rouge">cs107e.github.io</code> repository in your <code class="highlighter-rouge">cs107e_home</code> and do a <code class="highlighter-rouge">git pull</code> to ensure your courseware files are up to date.</p>

<p>To get the assignment starter code, change to your local repository, fetch any changes from the remote and switch to the assignment basic branch:</p>
<div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>$ cd ~/cs107e_home/assignments
$ git fetch origin
$ git checkout assign6-basic
</code></pre></div></div>

<p>Verify that your project has up-to-date versions of
your modules <code class="highlighter-rouge">gpio.c</code>, <code class="highlighter-rouge">timer.c</code>, <code class="highlighter-rouge">strings.c</code>, <code class="highlighter-rouge">printf.c</code>, <code class="highlighter-rouge">backtrace.c</code> and <code class="highlighter-rouge">malloc.c</code>, <code class="highlighter-rouge">keyboard.c</code> and <code class="highlighter-rouge">shell.c</code>.  Use <code class="highlighter-rouge">git merge</code> to gather any changes from other branches. For example, if you made a regrade submission on <code class="highlighter-rouge">assign5-basic</code>, have the <code class="highlighter-rouge">assign6-basic</code> branch checked out and use <code class="highlighter-rouge">git merge assign5-basic</code> to incorporate those updates.</p>

<p>If aiming to achieve the full system bonus in assignment 7, be sure that you are building and testing on your code, not the reference implementation. Consult the <code class="highlighter-rouge">Makefile</code> for information on configuring the build to use your modules.</p>

<p>The starter project contains the modules <code class="highlighter-rouge">fb.c</code>, <code class="highlighter-rouge">gl.c</code>, and <code class="highlighter-rouge">console.c</code>, the application program <code class="highlighter-rouge">apps/console_shell.c</code> and the test program <code class="highlighter-rouge">tests/test_gl_console.c</code>. You will edit <code class="highlighter-rouge">fb.c</code>, <code class="highlighter-rouge">gl.c</code>, and <code class="highlighter-rouge">console.c</code> to implement the required functions. You can add tests to <code class="highlighter-rouge">tests/test_gl_console.c</code>. The program <code class="highlighter-rouge">apps/console_shell.c</code> is used unchanged as a sample application which tests your console program.</p>

<p>The <code class="highlighter-rouge">make install</code> target of the Makefile builds and runs the sample application <code class="highlighter-rouge">apps/console_shell.bin</code>. The <code class="highlighter-rouge">make test</code> target builds and runs the test program <code class="highlighter-rouge">tests/test_gl_console.bin</code>. With no argument, <code class="highlighter-rouge">make</code> will build both, but not run.</p>

<h2 id="basic-section">Basic section</h2>

<h3 id="1-framebuffer">1) Framebuffer</h3>

<p>The base layer of graphics support is implemented in the <code class="highlighter-rouge">fb</code> module which manages the framebuffer and communicates with the GPU using our provided <code class="highlighter-rouge">mailbox</code> module. The functions exported from <code class="highlighter-rouge">fb</code> are:</p>

<ul>
  <li><code class="highlighter-rouge">void fb_init(unsigned int width, unsigned int height, unsigned int depth_in_bytes, fb_mode_t mode)</code></li>
  <li><code class="highlighter-rouge">void* fb_get_draw_buffer(void)</code></li>
  <li><code class="highlighter-rouge">void fb_swap_buffer(void)</code></li>
  <li>simple getters for fb settings: <code class="highlighter-rouge">fb_get_width</code>, <code class="highlighter-rouge">fb_get_height</code>, …</li>
</ul>

<p>The header file <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/fb.h">fb.h</a> documents the operation of each function.</p>

<p>The starter version of <code class="highlighter-rouge">fb_init()</code> contains the code from lab6 to configure the framebuffer in single-buffered mode. In this mode, there is only one buffer. All drawing takes place in that one buffer and is immediately displayed on screen. The framebuffer’s virtual size is configured to be equal to the physical size.</p>

<p>You will extend <code class="highlighter-rouge">fb_init</code> to support configuring the framebuffer in double-buffered mode. In this mode, the virtual height is set to be twice the physical height; this makes space for two screen-size buffers in the one virtual framebuffer. The lower half corresponds to one buffer and the upper half is the other. We refer to the buffer currently displayed on-screen as the ‘front’ buffer and the other buffer is the ‘back’ or ‘draw’ buffer. In double-buffered mode, all drawing is done to the back buffer and when ready, swapping the back buffer to front gives a smooth on-screen transition.</p>

<p>To implement <code class="highlighter-rouge">fb_swap_buffer</code> you change which half of the virtual framebuffer is frontmost (displayed) by changing the Y offset from 0 to the physical height (or vice versa). To make this change, set the <code class="highlighter-rouge">y_offset</code> in the fb struct and write to the mailbox to inform the GPU to update.</p>

<p>After each call to <code class="highlighter-rouge">mailbox_write</code>, follow up with a call to <code class="highlighter-rouge">mailbox_read</code> to acknowledge the GPU’s response. If you forget this, the mailbox queue eventually fills up and the system will hang.</p>

<p>The <code class="highlighter-rouge">tests/test_gl_console.c</code> test program has one very rudimentary test for the <code class="highlighter-rouge">fb</code> module. Be sure to augment the test program with your own tests that further exercise the functionality of the module. Your graphics library will be layered on the framebuffer and you need to be sure this foundation is solid before moving on.</p>

<h3 id="2-graphics-primitives">2) Graphics primitives</h3>
<p>The graphics library layers on the framebuffer and provides higher-level drawing primitives to set and get the color of a pixel, draw filled rectangles, and display text.</p>

<p>Read the header file
<a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/gl.h">gl.h</a> for documentation of the basic drawing functions:</p>

<ul>
  <li><code class="highlighter-rouge">void gl_init(unsigned int width, unsigned int height, gl_mode_t mode)</code></li>
  <li><code class="highlighter-rouge">void gl_draw_pixel(int x, int y, color_t c)</code></li>
  <li><code class="highlighter-rouge">color_t gl_read_pixel(int x, int y)</code></li>
  <li><code class="highlighter-rouge">void gl_draw_rect(int x, int y, int w, int h, color_t c)</code></li>
  <li><code class="highlighter-rouge">void gl_clear(color_t c)</code></li>
  <li><code class="highlighter-rouge">color_t gl_color(unsigned char r, unsigned char g, unsigned char b)</code></li>
  <li>simple getters for gl settings: <code class="highlighter-rouge">gl_get_width</code>, <code class="highlighter-rouge">gl_get_height</code>, …</li>
</ul>

<p>Review the provided <code class="highlighter-rouge">gl_init</code> code to initialize the framebuffer for 32-bit depth. Each pixel stores a 4-byte BGRA color.</p>

<p>Start by knocking out the simple getter functions that wrap the underlying <code class="highlighter-rouge">fb</code> functions to provide a consistent <code class="highlighter-rouge">gl</code> interface for the client. The graphics routines call into <code class="highlighter-rouge">fb</code>, but the client doesn’t need to know this. The client calls <code class="highlighter-rouge">gl_init</code> and <code class="highlighter-rouge">gl_draw....</code>, without any direct use of <code class="highlighter-rouge">fb</code>.</p>

<p>Next, move on to implement the <code class="highlighter-rouge">gl_draw_pixel</code> and <code class="highlighter-rouge">gl_read_pixel</code> functions. These set and get the color of an individual pixel. It is convenient to treat the framebuffer pixel data as a 2-D array and access pixels by x,y coordinates.  Review the syntax for multi-dimensioned arrays in the framebuffer lecture and the exercises of <a href="/labs/lab6">Lab 6</a>. Take care to compute the location of a pixel’s data in the framebuffer based on <strong>pitch</strong>, not <strong>width</strong>, because the GPU may have made each row a little wider than requested for reasons of alignment.</p>

<p>When accessing the pixel data, be ever mindful that C provides no bounds-checking on array indexes. If you write to an index outside the bounds of the framebuffer, you step on other memory in active use by the GPU, with various dire consequences to follow. It is imperative that your functions detect and reject an attempt to read or write a pixel location that is out-of-bounds.</p>

<p>Filling a rectangle (<code class="highlighter-rouge">gl_draw_rect</code>) can be implemented as a nested loop to draw each pixel and clearing the screen (<code class="highlighter-rouge">gl_clear</code>) fills a screen-sized rectangle. Be sure to draw only those pixels which lie within the framebuffer bounds. You can enforce clipping by calling <code class="highlighter-rouge">gl_draw_pixel</code> for each pixel and letting that function sort out whether the pixel is in bounds. This simple approach is easy to get correct.</p>

<p>An alternate approach for clipping would be for <code class="highlighter-rouge">gl_draw_rect</code> to first intersect the rectangle bounds with the screen bounds and proceed to color only the pixels in the intersection, obviating the need for a repeated per-pixel check for in bounds. This code would run much faster.</p>

<p>Speaking of performance, thus far we have ignored it as our programs have all run acceptably fast without special effort.  Now that we are writing graphics code, we can hit bottlenecks that cry out for attention. There can <strong>lots</strong> of pixels to process (2 million in a 1920x1080 display), so a <code class="highlighter-rouge">gl_clear</code> that executed 50 instructions per pixel would keep the Pi occupied for a full 5 seconds!  Reducing the number of instructions executed per pixel and streamlining the inner loop body will make a significant improvement.</p>

<p>For this assignment, we continue to prioritize correct functionality over efficiency, so it’s fine to go with a simple-and-slow approach for now. We expect that your console will be sluggish and it will miss keys that are typed while it is in the middle of drawing. You’ll fix this up in the final assignment by employing a mechanism for sharing the CPU during a long-running operation.</p>

<p>Some testing is now in order! Fire up the <code class="highlighter-rouge">tests/test_gl_console.c</code> test program and observe your graphics routines in action. Draw something that makes you happy! <a href="https://en.wikipedia.org/wiki/SMPTE_color_bars">SMPTE color bars</a>, the mandelbrot set, an animating automaton, crazy psychedelic patterns, …</p>

<h3 id="3-fonts-and-text-drawing">3) Fonts and text-drawing</h3>
<p>The final two functions to implement for the graphics library are:</p>

<ul>
  <li><code class="highlighter-rouge">void gl_draw_char(int x, int y, int ch, color_t c)</code></li>
  <li><code class="highlighter-rouge">void gl_draw_string(int x, int y, const char* str, color_t c)</code></li>
</ul>

<p>Lab 6 introduced you to the <a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/font.h">font.h</a>  module that manages the font image data.  A font has one combined bitmap consisting of glyphs for all characters, from which it can extract individual character images.</p>

<p><code class="highlighter-rouge">gl_draw_char</code> will use <code class="highlighter-rouge">font_get_char</code> to obtain the character image
and then draws each ‘on’ pixel.</p>

<p><code class="highlighter-rouge">gl_draw_string</code> is simply a loop that calls <code class="highlighter-rouge">gl_draw_char</code> for each character, left to right in a single line.</p>

<p>Just as you did previously, ensure that you clip all text drawing to the bounds of the framebuffer.</p>

<p>Edit the test program to draw yourself a congratulatory message and add a variety of tests that exercise text drawing.  You’re now ready to tackle the console.</p>

<h3 id="4-console">4) Console</h3>
<p>The console module uses the text-drawing functions of the graphics library to drive a monitor as a graphical output device. The console has these public functions:</p>

<ul>
  <li><code class="highlighter-rouge">void console_init(unsigned int nrows, unsigned int ncols)</code></li>
  <li><code class="highlighter-rouge">void console_clear(void)</code></li>
  <li><code class="highlighter-rouge">int console_printf(const char *format, ...)</code></li>
</ul>

<p>These functions are documented in the header file
<a href="https://github.com/cs107e/cs107e.github.io/blob/master/cs107e/include/console.h">console.h</a>                                                          .</p>

<p>The console module is a layer on top of the graphics
library, which itself is a layer on top of the framebuffer. The client interfaces with <code class="highlighter-rouge">console</code> by calling <code class="highlighter-rouge">console_init</code> and then
<code class="highlighter-rouge">console_printf</code>, without any direct use of <code class="highlighter-rouge">gl</code> or <code class="highlighter-rouge">fb</code>.</p>

<p>The console implementation stores the console contents, i.e., rows of text currently displayed, most likely using a 2-D array. The console also tracks the position of the cursor (insertion point). <code class="highlighter-rouge">console_init</code> initializes the contents to empty, <code class="highlighter-rouge">console_printf</code> adds text at the cursor position, and <code class="highlighter-rouge">console_clear</code> resets the contents to empty.</p>

<p>The <code class="highlighter-rouge">console_printf</code> function operates similarly to <code class="highlighter-rouge">printf</code> and uses your <code class="highlighter-rouge">vsnprintf</code> to prepare the formatted output. Once prepared, the characters to output are processed by the console one-by-one. Each ordinary character is inserted at the cursor position and the cursor advances. There are four special characters that require unique processing:</p>
<ul>
  <li><code class="highlighter-rouge">\b</code> : backspace (move cursor backwards one position)</li>
  <li><code class="highlighter-rouge">\r</code> : carriage return (move cursor to first column in current row)</li>
  <li><code class="highlighter-rouge">\n</code> : newline (move cursor down to first column of next row)</li>
  <li><code class="highlighter-rouge">\f</code> : form feed (clear contents and move cursor to home position in upper left)</li>
</ul>

<p>As it makes changes to the text contents, the console needs to update the display to match. You can make an incremental update as part of processing each character or postpone into a single refresh after processing a string of characters.</p>

<p>The console should also handle the operations for:</p>
<ul>
  <li>Horizontal wrapping: if there are too many characters to fit on the current row, automatically wrap the overflow to the next row. It is a nice touch for backspace to correctly on a wrapped row, but we won’t test this specific case in grading.</li>
  <li>Vertical scrolling: filling the bottommost row and starting a new one scrolls the text upwards, that is, all rows are shifted up by one. The top row scrolls off and the bottommost row now contains the text just added.</li>
</ul>

<p>Trying to implement all of the console functionality, including special cases, in one go is a tough way to proceed. Instead, break the work down into manageable steps. For example, start by implementing a console of a single row that only processes ordinary characters. Add processing for backspace and carriage return. Use the test program to find and fix any issues before proceeding.</p>

<p>Once your single row console is working, think about how you want to represent multiple rows. What kind of data structure will you use? How will you process newline and form feed?  Wrapping and scrolling add some unique complications. Think through what needs to happen for each and sketch out a plan before tackling that code.</p>

<p>Some ways of structuring the data will make the tasks much easier than others. We encourage you to talk
with your fellow students to discuss design tradeoffs.
Don’t feel bound to one design: if you start implementing your
approach and it starts seeming very difficult, with many hard edge cases,
you may want to consider a new design. Like much code in this class, 80% of
the effort is figuring out exactly what your code should do. Once you have that worked out, it can be direct matter to write it. Throwing away a messy first attempt and using what you learned from it to restart with a clean design is often the best path forward; far better than grinding along with a flawed approach.</p>

<h3 id="5-shell--console--magic">5) Shell + console = magic!</h3>

<p>The provided <code class="highlighter-rouge">console_shell.c</code> calls your <code class="highlighter-rouge">shell_init</code> passing
<code class="highlighter-rouge">console_printf</code> in place of the uart printf. With no change other than supplying a different output function, you now have a graphical version of the shell you wrote in assignment 5!</p>

<p>If the shell feels slow or drops keys as you’re typing, don’t
worry. We’ll fix that problem in the next assignment. Why might
the graphical shell be slow to process keys?</p>

<p>The video below demonstrates of our reference console. The shell is running on the Pi, user is typing on the PS/2 keyboard, and the output is displaying on the attached HDMI monitor.</p>

<iframe width="625" height="400" src="https://www.youtube-nocookie.com/embed/Emfmc0fAJXY?modestbranding=1&amp;version=3&amp;playlist=Emfmc0fAJXY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>

<h2 id="testing-and-debugging">Testing and debugging</h2>
<p>The framebuffer and graphics modules can be exercised by adding tests to the <code class="highlighter-rouge">tests/test_gl_console.c</code> program. The use of <code class="highlighter-rouge">assert</code> with <code class="highlighter-rouge">gl_read_pixel</code> can be used to confirm that expected color at a given pixel location.  You can also run the test program and observe what is displayed to the monitor and visually confirm correctness.</p>

<p>For <code class="highlighter-rouge">console</code>, most of the testing focuses on correct operation of <code class="highlighter-rouge">console_printf</code>. You can edit the test program to feed all manner of format strings to <code class="highlighter-rouge">console_printf</code> to observe its handling of special characters, wrapping, and scrolling. Once you have worked through any kinks, try <code class="highlighter-rouge">apps/console_shell.c</code> program to test <code class="highlighter-rouge">console_printf</code> in the context of the graphical shell.</p>

<p>Let us sound a significant note of caution about the necessity to be vigilant whenever you access the framebuffer. The neighboring memory to the framebuffer contains critical data and should you erroneously write to an out of bounds location, you can cause all manner of havoc for the GPU. These transgressions can be punished in mysterious ways (screen garbage, a surprise reshowing of the Pi test pattern, crash/lockup of the GPU, etc), leading to much sadness and frustration. Be conscious of the automatic scaling applied for pointer arithmetic/array access and always know the units a value is expressed in (bits? bytes? pixels?).</p>

<h2 id="extension-line-and-triangle-drawing">Extension: Line and triangle drawing</h2>
<p>Create an <code class="highlighter-rouge">assign6-extension</code> branch for this part of the work and make a separate pull request from your basic submission.</p>

<p>Extend the graphics library so that you can draw anti-aliased lines and
triangles. Implement the function prototypes as given in <code class="highlighter-rouge">gl.h</code>.</p>

<p>This extension is true extra credit
and requires you to learn about line drawing algorithms.
A good starting point is the
<a href="https://en.wikipedia.org/wiki/Line_drawing_algorithm">Wikipedia entry on line drawing</a>.</p>

<p>Your line drawing function should draw <em>anti-aliased</em> lines:</p>

<p><img src="images/antialiased.png" alt="Anti-aliased Lines" /></p>

<p>Implement triangle drawing by using your line-drawing routine to draw the anti-aliased outline of the triangle and then fill the interior with the user’s specified color.</p>

<p>Please note that pasting-and-modifying code you find online is not in the spirit of doing an extension and misrepresenting the work of others as your own is a violation of the Honor Code. You may read conceptual information and skim pseudocode to develop an understanding of an algorithm, but after reading, you should be able to put aside these references and write the C code yourself based on your own authentic understanding.</p>

<h2 id="submit">Submit</h2>
<p>The deliverables for <code class="highlighter-rouge">assign6-basic</code> are:</p>

<ul>
  <li>Implementation of the <code class="highlighter-rouge">fb.c</code>, <code class="highlighter-rouge">gl.c</code> and <code class="highlighter-rouge">console.c</code> modules</li>
  <li>Your tests in <code class="highlighter-rouge">tests/tests_gl_console.c</code></li>
</ul>

<p>Submit the finished version of your assignment by making a git “pull request”. Make separate pull requests for your basic and extension submissions.</p>

<p>The automated checks make sure that we can run your C
code and test and grade it properly, including swapping your tests for
ours.</p>

<p>CI verifies that:</p>

<ul>
  <li>
    <p><code class="highlighter-rouge">apps/console_shell.c</code> is unchanged</p>
  </li>
  <li>
    <p><code class="highlighter-rouge">make</code> and <code class="highlighter-rouge">make test</code> successfully build</p>
  </li>
  <li>
    <p><code class="highlighter-rouge">make test</code> also successfully builds with the unchanged version of the test program in the starter</p>
  </li>
</ul>

<h2 id="grading">Grading</h2>
<p>To grade this assignment, we will:</p>

<ul>
  <li>Verify that your submission builds correctly, with no warnings. Warnings and/or build errors result in automatic deductions. Clean build always!</li>
  <li>Run automated tests that exercise the functionality of your <code class="highlighter-rouge">fb.c</code>, <code class="highlighter-rouge">gl.c</code>and <code class="highlighter-rouge">console.c</code> modules. We will also interactively test your console program running with a PS/2 keyboard and HDMI monitor.</li>
  <li>Go over the tests you added to <code class="highlighter-rouge">tests/test_gl_console.c</code> and evaluate them for thoughtfulness and completeness in coverage.</li>
  <li>Review your code and provide feedback on your design and style choices.</li>
</ul>


  </div>
  <div class="toc-column col-lg-2 col-md-2 col-sm-2 hidden-xs">
    <div id="toc" class="toc" data-spy="affix" data-offset-top="0"></div>
  </div> 
</div>

  <script src="/_assets/tocbot.min.js"></script>
  <link rel="stylesheet" href="/_assets/tocbot.css">

  <script>
    tocbot.init({
      // Where to render the table of contents.
      tocSelector: '#toc',
      // Where to grab the headings to build the table of contents.
      contentSelector: '#main_for_toc',
      // Which headings to grab inside of the contentSelector element.
      headingSelector: 'h2, h3, h4',
    });
  </script>



  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>