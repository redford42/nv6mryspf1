<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/_assets/site.css" rel="stylesheet">
    <link href="/_assets/favicon.png" rel="icon" type="image/png">
    <title>CS107E Powering the Raspberry Pi A+</title>
  </head>

<body>
  <!-- Include this HTML partial to set up navbar -->

<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
    <!-- hamburger -->
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>

  <img class="navbar-brand navbar-brand-image" src="/_assets/berry.png" >
  <a class="navbar-brand" href="/">CS107e Winter 2020</a>
  </div>

  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">Home</a></li>
      <li><a href="/assignments">Assignments</a></li>
      <li><a href="/labs">Labs</a></li>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Resources
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/about/">About the course</a></li>
            <li><a href="/policies/">Policies</a></li>
            <li><a href="/schedule/">Schedule</a></li>
            <li><a href="/guides/">Guides</a></li>
            <li><a href="/project_gallery/">Project gallery</a></li>
            <li><a href="/demos/">External demos</a></li>
            <li><a href="/resources/">External resources</a></li>
          </ul>
        </li>
      <li><a href="https://github.com/cs107e/cs107e.github.io">Repository</a></li>
    </ul>
  </div>
</nav>


  <div class="container" style="max-width:55em;">
    <h1 class="title">Powering the Raspberry Pi A+</h1><hr>
    
  <p><em>Written by Pat Hanrahan</em></p>

<p><strong>Warning: This guide applies to the newest A+ and B+ models,
and not the older A and B models</strong></p>

<h3 id="powering-using-the-usb-serial-breakout-board">Powering using the usb-serial breakout board</h3>

<p>The way we will power the Pi in this course 
is to use the usb-serial breakout board.
The header on the breakout board has pins labeled 5V and GND.
On some breakout boards, the 5V pin may be labeled VCC;
if you are uncertain, check the voltage using a multimeter.
Some breakout boards will also have a pin labeled 3.3V.
The Pi will not run properly if you power it from 3.3V.</p>

<p>Power and ground are connected 
to the Raspberry Pi’s GPIO pins as shown.</p>

<p><img src="../images/power.usb.serial.jpg" alt="usb serial cable" /></p>

<p>The power LED is lit if the Raspberry Pi is receiving power.</p>

<p>Raspberry Pi A+ does not need much power. 
We can measure how much power the Raspberry Pi requires
by using an inline power meter.</p>

<p><img src="../images/power.meter.jpg" alt="Mac usb" /></p>

<p>The usb specification says that a usb port should supply 5V,
and up to 500 mA of current.
Our experiment shows that the usb port has a voltage of 4.72V
and is supplying 80 mA of current.
The total power being consumed is 0.37 W.
That is not a lot of power!</p>

<p>Note, however, that the power used 
may go up if you are using LEDs and other peripherals.</p>

<h3 id="powering-using-a-micro-usb-cable">Powering using a micro-usb cable</h3>

<p>Another way to power the Raspberry Pi is with a micro-usb
cable connected to a usb port on a laptop.</p>

<p><img src="../images/power.usb.laptop.jpg" alt="Mac usb" /></p>

<p>Note that there are different types of usb connectors.
The Raspberry Pi uses a micro-usb connector.</p>

<p>You can also directly power it with an AC adapter. 
The same kind that you use to charge your phone.</p>

<p><img src="../images/power.usb.ac.adapter.jpg" alt="micro-usb ac-adater" /></p>

<p>Note that there is a large USB port
on the bottom of the Raspberry Pi board.
Connecting a power source to the large usb connector 
will <em>not</em> power the Pi.</p>





  </div>
  <div class="footer navbar-default navbar-static-bottom">
    <p style="font-size: 65%; color:green; text-align:center;">
      <i>CS107e Winter 2020 &middot; Site generated 2020-04-19 20:35</i>
  </p>
</div> 
</body>

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</html>